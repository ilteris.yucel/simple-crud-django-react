from rest_framework import urlpatterns
from rest_framework import routers
from backend_api.views import MovieViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'movies', MovieViewSet, basename='movie')
urlpatterns = router.urls
